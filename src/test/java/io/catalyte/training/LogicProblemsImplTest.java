package io.catalyte.training;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/*
@Author: Jazmarie Moncrieft
@Class: Create Opportunity Charlotte Cohort 1
@Date: 12/15/2022
This is a series of methods that meet the requirements explained in the Intermediate Programming Project v1.
The test for these methods are in the testing class.
 */

public class LogicProblemsImplTest {
  //TODO: Implement all requirements as specified in the requirements document

    private LogicProblemsImpl logicObject = new LogicProblemsImpl();

    @Test
    public void averageTest() {
        int[] scores = {80, 95, 100, 98, 100};
        Double expected = 94.60;

        assertEquals(expected, logicObject.average(scores));

    }

    @Test
    public void negativeInt() {
        int[] scores = {80, 95, 100, -98, 100};

        assertThrows(IllegalArgumentException.class, () -> logicObject.average(scores));

    }

    @Test
    public void emptyArray() {
        int[] scores = {};
        Double expected = 00.00;

        assertEquals(expected, logicObject.average(scores));
    }


    @Test
    public void lengthOfLastWord() throws Exception {
        String text = "test this string";
        int expected = 6;

        assertEquals(expected, logicObject.lastWordLength(text));
    }


    @Test
    public void emptyString() {
        String text = "";

        assertThrows(IllegalArgumentException.class, () -> logicObject.lastWordLength(text));

    }

    @Test
    public void blankString() throws Exception {
        String text = " ";
        int expected = 0;

        assertEquals(expected, logicObject.lastWordLength(text));

    }

    @Test
    public void stringGroupingTest() {
        String[] mainArray = {"arrange", "act", "assert", "ace"};
        List<String> endWithEList = Arrays.asList("arrange", "ace");
        List<String> endWithTList = Arrays.asList("act", "assert");
        List<List<String>> groupedList = new ArrayList<>();
        groupedList.add(endWithEList);
        groupedList.add(endWithTList);

        assertEquals(groupedList, logicObject.groupStrings(mainArray));
    }

    @Test
    public void emptyArrayTest() {
        String[] emptyArray = new String[0];
        List<List<String>> expected = new ArrayList<List<String>>();

        assertEquals(expected, logicObject.groupStrings(emptyArray));

    }

    @Test
    public void emptyStringTest() {
        String[] emptyString = {"dog", "cat", "horse", ""};

        assertThrows(IllegalArgumentException.class, () -> logicObject.groupStrings(emptyString));
    }

    @Test
    public void distinctLadderRungsTest3(){
        int rungs = 3;
        BigDecimal expected = new BigDecimal(3);

        assertEquals(expected, logicObject.distinctLadderPaths(rungs));
    }
    @Test
    public void distinctLadderRungsTest100() {
        BigInteger answer = new BigInteger("573147844013817084101");
        int rungs = 100;
        BigDecimal expected = new BigDecimal(answer);

        assertEquals(expected, logicObject.distinctLadderPaths(rungs));
    }

    @Test
    public void distinctLadderRungsTest0() {
        int rungs  = 0;
        BigDecimal expected = BigDecimal.ZERO;

        assertEquals(expected, logicObject.distinctLadderPaths(rungs));
    }

    @Test
    public void distinctLadderRungsTestNegative() {
            assertThrows(IllegalArgumentException.class, () -> logicObject.distinctLadderPaths(-5));
    }



}


