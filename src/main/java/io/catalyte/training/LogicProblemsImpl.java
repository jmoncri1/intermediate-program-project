package io.catalyte.training;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/*
@Author: Jazmarie Moncrieft
@Class: Create Opportunity Charlotte Cohort 1
@Date: 12/15/2022
This is a series of methods that meet the requirements explained in the Intermediate Programming Project v1.
The test for these methods are in the testing class.
 */

public class LogicProblemsImpl implements LogicProblems {
  //TODO: Implement all requirements as defined in the project description.

   /**
    *
    *@param scores- list of student scores.
    *@return average- returns average of student scores.
    */
    @Override
    public Double average(int[] scores)
    {
        if(scores.length == 0) {
            return 00.00;
        }
        OptionalDouble average = Arrays.stream(scores).average();
        for (int score: scores)
        {
            if (score < 0)
            {
                throw new IllegalArgumentException("scores must be positive");
            }

        }
        average = OptionalDouble.of((double)Math.round(average.getAsDouble() * 100.0)/100.0);

        return average.getAsDouble();

    }

    /**
     *
     * @param text- receives a string of text
     * @return the length of the last word in the string of text
     */
    @Override
    public int lastWordLength(String text)
    {

        if(text.isEmpty())
        {
            throw new IllegalArgumentException("input must not be an empty string");
        }
        if(text.isBlank())
        {
            return 0;
        }

        text = text.trim();

        String lastWord = text.substring(text.lastIndexOf(" "));
        int lengthOfLastWord = lastWord.length() - 1;
        return lengthOfLastWord;
    }



    public static Map<Integer, BigInteger> map = new HashMap<>();
    static{
        map.put(1, BigInteger.valueOf(1));
        map.put(2, BigInteger.valueOf(2));
    }

    /**
     *
     * @param rungs- and integer representing the number of rungs on a ladder
     * @return a big decimal representing all possible combination paths of climbing a ladder with the given number of rungs.
     */
    @Override
    public BigDecimal distinctLadderPaths(int rungs) {

        if (rungs == 0){
            return BigDecimal.ZERO;
        }

        if (rungs < 0) {
            throw new IllegalArgumentException("ladders can't have negative rungs");
        }

        if(map.containsKey(rungs)) {
            return new BigDecimal(map.get(rungs));
        }

        BigInteger firstNumber, secondNumber;

        if (map.containsKey(rungs - 1)) {
            firstNumber = map.get((rungs - 1));
        }
        else {
            firstNumber = distinctLadderPaths(rungs - 1).toBigInteger();
        }

        if(map.containsKey(rungs - 2)) {
            secondNumber = map.get(rungs-2);
        }
        else {
            secondNumber = distinctLadderPaths(rungs - 2).toBigInteger();
            map.put(rungs - 2, secondNumber);
        }
        return new BigDecimal(firstNumber.add(secondNumber));

    }

    /**
     *
     * @param strs a list of strings are provided.
     * @return a list of lists of strings grouped by similar beginning and ending letters
     */

    @Override
    public List<List<String>> groupStrings(String[] strs)
    {

        if (strs.length == 0)
        {
            return new ArrayList<>();
        }

        List<List<String>> mainList = new ArrayList<>();
        List<String> endWithE = new ArrayList<>();
        List<String> endWithT = new ArrayList<>();
        for (int i = 0; i < strs.length; i++)
        {
            String word = strs[i];

            if (word.isEmpty())
            {
                throw new IllegalArgumentException("strings must not be empty");
            }
            char firstLetter = word.charAt(0);
            char lastLetter = word.charAt(word.length() - 1);


            if (firstLetter == 'a' && lastLetter == 'e')
            {
                endWithE.add(word);
            }
            if (firstLetter == 'a' && lastLetter == 't')
            {
                endWithT.add(word);
            }
        }
        mainList.add(endWithE);
        mainList.add(endWithT);
        return mainList;
    }


}


