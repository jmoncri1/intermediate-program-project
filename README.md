## Intermediate Programming Project

#### The "LogicProblems" class is the interface containing the methods for testing.</h4>

#### The "LogicProblemsImpl" contains the body/code for the methods.

    The methods are:
    
    1- Average of student scores

    2- Length of the last word in a given string

    3- Calculating the possible combination of the number of step required to climb a ladder with a given number of rungs.

    4- When a list of strings are provided, the method groups the strings into their own list of words with the same beginning and ending letter.

#### "LogicProblemsImplTest" contain the various test as required by the given project description
    
#### To test the methods, open the "LogicProblemsImplTest" and you will be able to run each labelled test for the methods.
    These are the test:

    1- Average of student scores-test for average of given array of ints, returning 0 if array is empty, and throw exception message if in is negative.
    2- Length of the last word in a given string-returns the length of the last word. If the string is only whitespace, 0 is returned. 
        If the string is empty, exception message is displayed.
    3- Calculating the possible combination of the number of step required to climb a ladder with a given number of rungs. 
        The test tries 3 rungs and 100 rungs. If the rungs is 0, then 0 is returned. 
        If the number of rungs is a negative number, an exception message is displayed.
    4- When a list of strings are provided, the method groups the strings into their own list of words with the same beginning and ending letter.
        The test tries all words that begin with "a" and end with "e."
        The second group are words that begin with "a and end with "t."

<h5>My linting process included assuring consistent camel case variable naming, K & R curly bracket styling,
    and use of IntelliJ IDE error messages and suggestions for cleaner code</h5>








# Intermediate Programming Project Template
Intermediate Programming Project Template is used to give trainees a base project for demonstrating logical thinking.
## Installation
Fork the repository to your specified work directory, then clone it down
```bash
git clone https://gitlab.com/jmoncri1/intermediate-program-project.git
```
## Usage
Fill out LogicProblemsImpl with working solutions to the interface. Follow all requirements specified in the project instructions.
## Contributing
Feedback is welcome. If you see issues, or have ideas for improvement, please submit feedback!